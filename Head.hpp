

#ifndef PROJECT_HEAD_HPP
#define PROJECT_HEAD_HPP

#endif //PROJECT_HEAD_HPP
#include <iostream>

using namespace std;
const int N = 1000;

namespace fun
{
    void InputnMinMax(int KolVo, int  mas[N], int min, int max)  // поиск мин и макс значения, ввод самой последовательности и вывода макс и мин значений
    {
        cout<<"Введите саму последовательность: ";

        for(int x = 0 ; x < KolVo ; x++)
        {
            cin>>mas[x];
            if(min > mas[x])
                min = mas[x];
            if(max < mas[x])
                max = mas[x];
        }
        cout<<"min: ";
        cout<<min<<endl;
        cout<<"max: ";
        cout<<max<<endl;
    }

    void Output(int KolVo, int mas[N])
    {
        for(int x = 0 ; x < KolVo ; x++)
            cout<<mas[x]<<" ";
        cout<<endl;
    }

    void OutputnSort(int KolVo , int mas[N]) //сортировка последовательности  по возрастанию и ее вывод
    {

        cout<<"Последовательность по возрастанию: ";

        for(int i = 0 ; i < KolVo - 1 ; i++)
        {
            for(int  j = i + 1 ; j < KolVo ; j ++)
            {
                if(mas[i] > mas[j])
                    swap(mas[i],mas[j]);
            }
        }

        Output(KolVo, mas);

    }

}

